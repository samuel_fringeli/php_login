<?php include 'header.php'?>
<div class="container">
    <?php
    if (isset($_GET['registered'])) {
        echo '<div class="alert alert-success">Vous avez bien été enregistré !</div>';
    }
    if (isset($_GET['connected'])) {
        echo '<div class="alert alert-success">Vous avez bien été connecté !</div>';
    }
    
    include 'guestbook.php';
    ?>
</div>
<?php include 'footer.php'?>