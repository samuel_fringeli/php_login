<?php include 'header.php'?>

<div class="container">
    <form id="registation" method="post" action="user_add.php">
        <p class="alert alert-info">Bienvenue dans le formulaire de création de compte !</p>
        <label>Nom : </label><input class="form-control" type="text" name="nom" placeholder="Noth" required=""/>
        <label>Prénom : </label><input class="form-control" type="text" name="prenom" placeholder="Samuel" required=""/>
        <label>Mail : </label><input class="form-control" type="email" name="adresse_mail" placeholder="exemple@exemple.com" required=""/>
        <label>Pseudo : </label><input class="form-control" type="text" name="username" placeholder="nothsamuel" required=""/>
        <label>Password : </label><input class="form-control" type="password" name="password" placeholder="Mot de passe" required=""/>
        <input class="btn btn-primary" type="submit" value="Envoyer"/>
    </form>
</div>

<?php include 'footer.php'?>