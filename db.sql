-- 1. création des tables
CREATE TABLE utilisateurs( 
	id int AUTO_INCREMENT,
	nom varchar(50),
	prenom varchar(50),
	adresse_mail varchar(50),
	username varchar(50) UNIQUE,
	password varchar(50),
	PRIMARY KEY (id)
);

CREATE TABLE messages(
    id int AUTO_INCREMENT,
    titre varchar(50),
    contenu text,
    date_publication datetime,
    auteur int,
    PRIMARY KEY (id),
    FOREIGN KEY (auteur) REFERENCES utilisateurs(id)
);