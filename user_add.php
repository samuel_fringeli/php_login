<?php include 'connect.php';

$begin = 'INSERT INTO utilisateurs(';
$end = 'VALUES(';
$values = array();
$valid_post = true;

foreach ($fields as $key => $field) {
    if ($key != count($fields)-1) {
        $begin .= $field.', ';
        $end .= ':'.$field.', ';
    } else {
        $begin .= $field.') ';
        $end .= ':'.$field.');';
    }
    if (!isset($_POST[$field])) {
        $valid_post = false;
    }
}

if ($valid_post) {
   foreach ($fields as $field) {
       $values[$field] = $_POST[$field];
   }
}

$req = $bdd->prepare($begin.$end);
$req->execute($values);

$_SESSION['username'] = $_POST['username'];
header("Location:index.php?registered");
exit();
?>