<?php include 'connect.php';

if(isset($_POST['username'], $_POST['password'])) {
    $req = $bdd->prepare('SELECT * FROM utilisateurs WHERE username = ?');
    $req->execute(array($_POST['username']));
    $password = $req->fetch()['password'];
    
    if ($password == $_POST['password']) {
        $_SESSION['username'] = $_POST['username'];
        header("Location: index.php?connected");
    } else {
        header("Location: login.php?wrong");
    }
    exit();
}

?>