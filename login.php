<?php include 'header.php'?>

<div class="container">    
    <form id="login" method="post" action="login_verify.php">
        <p class="alert alert-info">Bienvenue dans le formulaire de login !</p>
        <?php 
        if (isset($_GET['wrong'])) {
            echo '<p class="alert alert-danger">Ce mot de passe ne correspond pas à ce nom d\'utilisateur</p>';
        }
        ?>
        <label>Username : </label><input class="form-control" type="text" name="username" placeholder="Username"/>
        <label>Password : </label><input class="form-control" type="text" name="password" placeholder="Password"/>
        <input class="btn btn-primary" type="submit" value="Envoyer"/>
    </form>
</div>

<?php include 'footer.php'?>