<?php include 'connect.php'?>
<?php if (!isset($_SESSION['username'])) {
    header("Location: login.php");
    exit();
} 

if (isset($_POST['titre'], $_POST['contenu'])) {
    $req = $bdd->prepare("INSERT INTO messages(titre, contenu, date_publication, auteur) SELECT :titre, :contenu, NOW(), id FROM utilisateurs WHERE username = :username");
    $req->execute(array(
        'titre' => $_POST['titre'],
        'contenu' => $_POST['contenu'],
        'username' => $_SESSION['username']
    ));
    header("Location: index.php");
    exit();
}
?>

<form id="message" method="post" action="guestbook.php">
    <p class="alert alert-info">Bienvenue dans le formulaire de création d'un message !</p>
    <label>Titre : </label><input class="form-control" type="text" name="titre" placeholder="titre"/>
    <label>Contenu : </label><input class="form-control" type="text" name="contenu" placeholder="contenu"/>
    <input class="btn btn-primary" type="submit" value="Envoyer"/>
</form>

<div class="container">    
<?php 

if (isset($_SESSION['username'])) {
    
    $req = $bdd->prepare('SELECT * FROM messages JOIN utilisateurs ON auteur = utilisateurs.id ORDER BY date_publication ASC');
    $req->execute(array());
    
    echo '<br>';
    while ($donnees = $req->fetch()) {
    	echo '<b>Écrit le : </b>' . $donnees['date_publication'] . ' par <b>' . $donnees['prenom'] . ' ' . $donnees['nom'] . '</b> alias <b>' . $donnees['username'] . '</b><br> <b>Message : </b>' . $donnees['contenu'] . '<br><hr>';
    }
}
    
?>

</div>
