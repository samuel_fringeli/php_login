<?php include 'connect.php';
include 'header.php';

if (isset($_POST['nom'], $_POST['prenom'], $_POST['adresse_mail'])) {
    $req = $bdd->prepare("UPDATE utilisateurs SET nom = ?, prenom = ?, adresse_mail = ? WHERE username = ?");
    $req->execute(array($_POST['nom'], $_POST['prenom'], $_POST['adresse_mail'], $_SESSION['username']));
}

$reponse = $bdd->query('SELECT * FROM utilisateurs WHERE username = "'.$_SESSION['username'].'"');
$data = $reponse->fetch();

?>
<div class="container">    
    <form id="modify" method="post" action="profil.php">
        <p class="alert alert-info">Bienvenue dans le formulaire de modification du profil !</p>
        <label>Nom : </label><input class="form-control" type="text" name="nom" value="<?=$data['nom']?>" placeholder="Nom"/>
        <label>Prénom : </label><input class="form-control" type="text" name="prenom" value="<?=$data['prenom']?>" placeholder="Prénom"/>
        <label>Email : </label><input class="form-control" type="text" name="adresse_mail" value="<?=$data['adresse_mail']?>" placeholder="exemple@exemple.com"/>
        <input class="btn btn-primary" type="submit" value="Modifier"/>
    </form>
</div>

<?php include 'footer.php'?>